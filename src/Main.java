public class Main {
    public static void main(String[] args) {
        BasketballPlayer basketballPlayer = new BasketballPlayer("Danzhi",16, 12, 201);
        FootballPlayer footballPlayer = new FootballPlayer("Ayanokodzhi", 15, 20, 5, 3, 1);
        HockeyPlayer hockeyPlayer = new HockeyPlayer("Kazuma", 18, 0);

        basketballPlayer.dunk();

        System.out.println(footballPlayer.getName()+" "+footballPlayer.calculateEfficiency()+" efficient");
    }
}