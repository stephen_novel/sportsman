public class BasketballPlayer extends Sportsman{
    private Integer height;
    public BasketballPlayer(String name, Integer age, Integer count, Integer height) {
        super(name, age, count);
        this.height = height;
    }

    public void dunk(){
        System.out.println(getName() +" has dunked");
    }
}
