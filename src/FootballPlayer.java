public class FootballPlayer extends Sportsman{

    private Integer goals;

    private Integer assists;

    private Integer interceptions;

    public FootballPlayer(String name, Integer age, Integer count, Integer goals, Integer assists, Integer interceptions) {
        super(name, age, count);
        this.goals = goals;
        this.assists = assists;
        this.interceptions = interceptions;
    }

    public int calculateEfficiency(){
        return (goals + assists + interceptions)/3;
    }
}
